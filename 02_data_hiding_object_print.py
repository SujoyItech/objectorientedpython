class MyClass:
    _hiddenVariable = 0
    def add(self, increment):
        self._hiddenVariable += increment
        print(self._hiddenVariable)

myObject = MyClass()
myObject.add(2)
myObject.add(5)
#print(myObject._hiddenVariable)

# We can access the value of hidden attribute by a tricky syntax:

class MyClass2:
    __hiddenVariable = 10

myObject2 = MyClass2 ()
#print(myObject2.__MyClass2__hiddenVariable)

class Test:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __repr__(self):
        return "Test a:%s b:%s" % (self.a, self.b)

    def __str__(self):
        return "From str method of Test: a is %s," \
              "b is %s" % (self.a, self.b)

t = Test(1234, 5678)
print(t)
print([t])
