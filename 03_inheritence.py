class Person(object) :
    def __init__(self, name):
        self.name = name

    def getName(self):
        return self.name

    def isEmployee(self):
        return False

class Employee(Person):
    def isEmployee(self):
        return True

emp = Person('Geek')
print(emp.getName(), emp.isEmployee())
emp = Employee('Geek2')
print(emp.getName(), emp.isEmployee())

# How to check if a class is subclass of another?

class Base:
    pass

class Drived(Base):
    pass

print(issubclass(Drived,Base))
print(issubclass(Base,Drived))

# Unlike Java and like C++, Python supports multiple inheritance

class Base1(object):
    def __init__(self, str):
        self.str1 = str
        print('Base 1')

class Base2(object):
    def __init__(self, str):
        self.str2 = str
        print('Base 2')

class DerivedBase(Base1,Base2):
    def __init__(self, str):
        Base1.__init__(self, str)
        Base2.__init__(self, str)
        print('Derived base')

    def printStr(self):
        print(self.str1,self.str2)

ob = DerivedBase('Sujoy')
ob.printStr()

#How to access parent members in a subclass?

class BaseParent(object):
    def __init__(self, x):
        self.x = x

class DriveBaseParent:
    def __init__(self, x, y):
        BaseParent.x = x
        self.y = y

    def printXY(self):
        print(BaseParent.x, self.y)

d = DriveBaseParent(10,20)
d.printXY()

