class MyClass:
    number = 0
    name = "noname"

def main():
    me = MyClass()
    me.number = 10000
    me.name = "Sujoy Nath"
    print(me.name + ' ' + str(me.number))

class Vector2D:
    x = 0.0
    y = 0.0
    def set(self,x,y):
        self.x = x
        self.y = y

def printVector2D():
    vec = Vector2D()
    vec.set(5, 6)
    print('x: ' + str(vec.x) + ' y: ' + str(vec.y))

class Pet:
    def __init__(self, name, age):
        self.name = name
        self.age = age

class Cat(Pet):
    def __init__(self, name, age):
        super().__init__(name, age)

def CatPrint():
    pet = Pet('Pet', 1)
    cat = Cat('Jess', 3)

    print('Is jess a cat ?' + str(isinstance(cat, Cat)))
    print('Is jess a pet ?' + str(isinstance(cat, Pet)))
    print('Is pet a pet ?' + str(isinstance(pet, Pet)))
    print(cat.name)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
    printVector2D()
    CatPrint()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
