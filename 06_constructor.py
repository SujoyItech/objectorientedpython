# Types of constructors :
#
# default constructor: The default constructor is a simple constructor which doesn’t accept any arguments.
# Its definition has only one argument which is a reference to the instance being constructed.
#
# parameterized constructor: constructor with parameters is known as parameterized constructor.
# The parameterized constructor takes its first argument as a reference to the instance being
# constructed known as self and the rest of the arguments are provided by the programmer.

#Example of default constructor :

class GeekforGeeks:
    def __init__(self):
        self.geek = "Geekforgeeks"

    def print_geek(self):
        print(self.geek)

obj = GeekforGeeks()
obj.print_geek()

# Example of the parameterized constructor :

class Addition:
    first = 0
    second = 0
    answer = 0
    def __init__(self, first, second):
        self.first = first
        self.second = second

    def display(self):
        print('First number = ' + str(self.first))
        print('Second number = ' + str(self.second))
        print('Addition of two number = ' + str(self.answer))

    def calculate(self):
        self.answer = self.first + self.second

obj = Addition(1000,2000)
obj.calculate()
obj.display()